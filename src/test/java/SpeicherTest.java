import static org.junit.Assert.*;

import org.junit.Test;

public class SpeicherTest {

	@Test
	public void setWertTest() throws InterruptedException {
		Speicher sp = new Speicher();
		sp.setWert(15);
		assertTrue(sp.isHatWert());
		}
	
	@Test
	public void getWertTest() throws InterruptedException{
		Speicher sp = new Speicher();
		sp.setWert(12);
		assertEquals(sp.getWert(), 12);
	}
	
	@Test
	public void isHatWertTest() throws InterruptedException {
		Speicher sp = new Speicher();
		sp.setWert(9);
		assertTrue(sp.isHatWert());
	}
	
}