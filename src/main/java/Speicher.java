/**
 * Die Klasse Speicher sichert eine Zahl vom Zaehler in den Speicher. 
 * 
 * @author Stephanie Kaswurm, Anna-Lena Klaus
	
 */

public class Speicher implements SpeicherIf {

    private int wert;
    private boolean hatWert = false;
    
    /**
	 * @return result
	 * @throws InterruptedException
	 */

    @Override
    public synchronized int getWert() throws InterruptedException {
        return wert;
    }

    @Override
    public synchronized void setWert(int wert) throws InterruptedException {
        this.wert = wert;
        hatWert = true;
    }

    @Override
    public boolean isHatWert() {
        return hatWert;
    }
}