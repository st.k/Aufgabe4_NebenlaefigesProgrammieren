/**
 * Die Klasse Zaehler zaehlt von min bis max, je nach mitgegebenen Argumenten und speichert die
 * Zahlen im Speicher. Die Argumente müssen mitgegeben werden!
 * 
 * @author Stephanie Kaswurm, Anna-Lena Klaus
 *
 */
	

public class Zaehler extends Thread {

    private Speicher speicher;
    private int max, min;

    /**
	 * 
	 * @param s: Das Speicherobject, das die aktuelle Zahl beinhaltet.
	 * 
	 * @param min: Der Startwert fuer den Zaehler.
	 * 
	 * @param max: Der Endwert fuer den Zaehler (einschliesslich).
	 *  
	 */
    
    
    Zaehler(Speicher s, int min, int max) {
        this.speicher = s;
        this.max = max;
        this.min = min;
    }

    /**
	 * Diese Run Methode zaehlt den Wert, mithilfe einer For-Schleife, im Speicher hoch - von min bis max
	 * (einschliesslich), mit einer jeweiligen Erhoehung um +1.
	 */
    
    @Override
    public void run() {
        try {


            for (int zaehler = min; zaehler <= max; ++zaehler) {
                synchronized (speicher){
                    speicher.setWert(zaehler);

                    speicher.notifyAll();
                    speicher.wait();
                }
            }


        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}